package xebia.r2d2

import java.sql.Connection
import java.util.concurrent.TimeUnit

import akka.actor._
import org.joda.time.DateTime

import scala.collection.mutable.{ HashMap ⇒ MutableHashMap, Queue ⇒ MutableQueue }
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

object ConnectionPoolActor {

  implicit class JodaWithDuration(first: DateTime) {
    def until(second: DateTime): Duration = {
      Duration(Math.abs(first.getMillis - second.getMillis), TimeUnit.MILLISECONDS)
    }
  }

  def props(instance: ConnectionPoolActor) = Props(instance)

  private[r2d2] case class ConnectionMetadata(connection: Connection, created: DateTime, lastCheckout: DateTime)
  private[r2d2] case class ConnectionRequest(ref: ActorRef, created: DateTime)

  private[r2d2] case object RequestConnection
  private[r2d2] case class ReturnConnection(connection: Connection)
  private[r2d2] case class CreatedConnection(metadata: ConnectionMetadata)
  private[r2d2] case class FailedCreatingConnection(t: Throwable)
  private[r2d2] case object TestConnections

  class ConnectionRequestException(message: String, cause: Throwable) extends Exception(message, cause) {
    def this(message: String) = {
      this(message, null)
    }
  }

  case class ConnectionRequestTimeoutException(duration: Duration)
    extends ConnectionRequestException(s"Connection request timed out, took longer than ${duration.toMillis}ms.")
  case class MaxConnectionRequestsExceededException(max: Int)
    extends ConnectionRequestException(s"Max number outstanding requests has been exceeded. Max is $max.")
}

trait ConnectionPoolActor extends ConnectionPool with Actor with ActorSystemContextSupport with ActorAskSupport
    with CurrentTime with BlockingExecutionContext with ConnectionPoolConfig with ActorLogging with ConnectionFactory {

  import xebia.r2d2.ConnectionPoolActor._

  val availableConnections = MutableQueue[ConnectionMetadata]()
  val usedConnections = MutableHashMap[Connection, ConnectionMetadata]()
  val connectionRequests = MutableQueue[ConnectionRequest]()

  def totalConnections = availableConnections.size + usedConnections.size

  private[this] def getConnection: Future[Connection] = self.ask(RequestConnection).mapTo[Connection]

  // Interface
  override def withConnection[T](block: (Connection) ⇒ Future[T]): Future[T] = {
    getConnection.flatMap { connection: Connection ⇒
      block(connection).andThen { case _ ⇒ returnConnection(connection) }
    }
  }

  override def receive: Receive = {
    case RequestConnection            ⇒ requestConnection(sender())
    case ReturnConnection(connection) ⇒ returnConnection(connection)
    case CreatedConnection(metadata) ⇒
      log.debug(s"Adding newly created connection (${metadata.connection}) to the queue of available connections")
      availableConnections.enqueue(metadata)
      handOutConnection()
    case FailedCreatingConnection(t) ⇒ checkoutFailure(t)
    case TestConnections             ⇒ maintenance()
  }

  {
    context.system.scheduler.schedule(initialDelay = 5.seconds, interval = 5.seconds, self, TestConnections)
  }

  private[this] def requestConnection(sender: ActorRef): Unit = {
    log.debug(s"New connection requested by $sender")
    if (connectionRequests.size >= maxPendingConnections) {
      log.debug(s"Maximum pending connection requests exceeded. Was ${connectionRequests.size}, max $maxPendingConnections")
      sender ! Status.Failure(new MaxConnectionRequestsExceededException(maxPendingConnections))
    } else {
      log.debug("Pending connection request added to queue")
      connectionRequests.enqueue(ConnectionRequest(sender, now))
      if (availableConnections.isEmpty) {
        log.debug("No connections available")
        if (totalConnections < poolSize) {
          log.debug(s"Maximum pool size ($poolSize) not reached. Creating new connection.")
          createNewConnection()
        }
        log.debug(s"Maximum pool size ($poolSize) reached.")
      } else {
        handOutConnection()
      }
    }
  }

  private[this] def returnConnection(connection: Connection): Unit = {
    log.debug(s"Returning connection $connection to the pool.")
    availableConnections.enqueue(usedConnections(connection))
    handOutConnection()
  }

  private[this] def createNewConnection(): Unit = {
    blockingAsync {
      createConnection
    }.onComplete {
      case Success(db) ⇒ self ! CreatedConnection(ConnectionMetadata(db, now, now))
      case Failure(t)  ⇒ self ! FailedCreatingConnection(t)
    }
  }

  private[this] def handOutConnection(): Unit = {
    if (connectionRequests.nonEmpty && availableConnections.nonEmpty) {

      val metadata = availableConnections.dequeue()
      val connectionRequest = connectionRequests.dequeue()

      if (metadata.connection.isClosed) {
        closeConnection(metadata.connection)
        handOutConnection()
      } else {
        log.debug(s"Handing connection (${metadata.connection}}) to requester (${connectionRequest.ref}}).")
        usedConnections += ((metadata.connection, metadata))
        connectionRequest.ref ! metadata.connection
      }
    }
  }

  private[this] def closeConnection(connection: Connection): Unit = {
    blockingAsync {
      connection.close()
    }
  }

  private[this] def checkoutFailure(t: Throwable) = {
    val failure = new ConnectionRequestException("Failed to acquire connection", t)
    if (connectionRequests.nonEmpty) {
      connectionRequests.dequeue().ref ! Status.Failure(failure)
    } else {
      //only log it when not sent back to a user to prevent double logging
      log.error(failure, "Error during creation of connection")
    }
  }

  private[this] def maintenance(): Unit = {
    log.debug("Running connection pool maintenance")
    withCurrentTime { now ⇒
      val expiredConnections = availableConnections.dequeueAll(metadata ⇒ hasConnectionExpired(metadata, now))
      expiredConnections.foreach(metadata ⇒ closeConnection(metadata.connection))
      val timedOutConnectionRequests = connectionRequests.dequeueAll { request ⇒
        (request.created until now) > connectionRequestTimeout
      }
      timedOutConnectionRequests.foreach { request ⇒
        log.debug(s"Cleaning up timed out (age ${request.created until now}) connection request and notifying requester (${request.ref}).")
      }
      if (timedOutConnectionRequests.nonEmpty) {
        timedOutConnectionRequests.foreach { meta ⇒
          meta.ref ! Status.Failure(ConnectionRequestTimeoutException(connectionRequestTimeout))
        }
      }
    }
  }

  private[this] def hasConnectionExpired(metadata: ConnectionMetadata, now: DateTime = now): Boolean = {
    withCurrentTime { now ⇒
      ((metadata.created until now) > maxConnectionAge) ||
        ((metadata.lastCheckout until now) > maxConnectionIdleTime)
    }
  }
}
