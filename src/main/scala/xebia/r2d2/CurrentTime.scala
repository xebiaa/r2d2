package xebia.r2d2

import org.joda.time.DateTime

trait CurrentTime {
  def now: DateTime
  def withCurrentTime[T](block: DateTime ⇒ T): T = block(now)
}
