package xebia.r2d2.defaults

import java.sql.{ Connection, DriverManager }
import java.util.Properties

import xebia.r2d2.ConnectionFactory

trait DefaultConnectionFactory extends ConnectionFactory { self: DefaultConnectionPoolConfig ⇒

  def postConfig(connection: Connection): Unit = {}

  override def createConnection: Connection = {
    val props = new Properties()
    props.put("user", username)
    props.put("password", password)

    val connection = DriverManager.getConnection(connectionString, props)
    postConfig(connection)
    connection
  }
}
