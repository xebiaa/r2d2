package xebia.r2d2

import scala.concurrent.duration.Duration

trait ConnectionPoolConfig {
  def poolSize: Int
  def maxConnectionAge: Duration
  def maxConnectionIdleTime: Duration
  def connectionRequestTimeout: Duration
  def maxPendingConnections: Int
}
