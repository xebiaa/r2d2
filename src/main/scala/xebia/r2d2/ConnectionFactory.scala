package xebia.r2d2

import java.sql.Connection

trait ConnectionFactory {
  def createConnection: Connection
}
