package xebia.r2d2

import scala.concurrent.{ Future, ExecutionContext }

trait BlockingExecutionContext {
  def blockingExecutionContext: ExecutionContext
  def blockingAsync[T](block: ⇒ T): Future[T] = Future(block)(blockingExecutionContext)
}
