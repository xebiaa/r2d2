package xebia.r2d2

import java.sql.Connection
import java.util.concurrent.Executors

import akka.actor.{ ActorRef, ActorSystem, Status }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit }
import org.joda.time.DateTime
import org.specs2.mock.Mockito
import org.specs2.mutable.{ After, Specification }

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration

class ConnectionPoolActorSpec extends Specification with Mockito {

  abstract class ConnectionPoolActorScope extends TestKit(ActorSystem("TestSystem")) with After with ImplicitSender {

    override def after: Any = system.shutdown()

    def withTestConnectionPool(instance: ⇒ ConnectionPoolActor)(block: ActorRef ⇒ Unit): Unit = {
      block(TestActorRef(instance))
    }

    trait TestConnectionPoolConfig extends ConnectionPoolConfig {
      override def maxPendingConnections: Int = 10
      override def connectionRequestTimeout: Duration = Duration("1s")
      override def maxConnectionIdleTime: Duration = Duration("1m")
      override def maxConnectionAge: Duration = Duration("1m")
      override def poolSize: Int = 10
    }

    trait TestConnectionFactory extends ConnectionFactory {
      override def createConnection: Connection
    }

    trait TestConnectionPoolActor extends ConnectionPoolActor with TestConnectionPoolConfig with TestConnectionFactory {
      override def now: DateTime = DateTime.now()
      override def system: ActorSystem = ConnectionPoolActorScope.this.system
      override def blockingExecutionContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(poolSize + 1))
    }
  }

  "The ConnectionPoolActor" should {
    "fail if the maximum number of pending connections has been exceeded" in new ConnectionPoolActorScope {
      // Setup default implementation with some twists
      val mockConnection = mock[Connection]
      def sut = new TestConnectionPoolActor {
        override def maxPendingConnections = 1
        override def poolSize = 1
        override def createConnection = mockConnection
      }
      withTestConnectionPool(sut) { ref ⇒
        ref ! ConnectionPoolActor.RequestConnection
        expectMsg(mockConnection)
        ref ! ConnectionPoolActor.RequestConnection
        ref ! ConnectionPoolActor.RequestConnection
        expectMsg(Status.Failure(ConnectionPoolActor.MaxConnectionRequestsExceededException(1)))
      }
    }

    "fail if having to wait too long for a connection" in new ConnectionPoolActorScope {
      // Setup default implementation with some twists
      val mockConnection = mock[Connection]
      def sut = new TestConnectionPoolActor {
        override def maxPendingConnections = 1
        override def poolSize = 1
        override def createConnection = mockConnection
      }
      withTestConnectionPool(sut) { ref ⇒
        ref ! ConnectionPoolActor.RequestConnection
        expectMsg(mockConnection)
        ref ! ConnectionPoolActor.RequestConnection
        expectMsg(
          Duration(5, "s"),
          Status.Failure(ConnectionPoolActor.ConnectionRequestTimeoutException(Duration("1s")))
        )
      }
    }

    "reuse a returned connection" in new ConnectionPoolActorScope {
      // Setup default implementation with some twists
      val mockConnection = mock[Connection]
      def sut = new TestConnectionPoolActor {
        override def maxPendingConnections = 1
        override def poolSize = 1
        override def createConnection = mockConnection
      }
      withTestConnectionPool(sut) { ref ⇒
        ref ! ConnectionPoolActor.RequestConnection
        expectMsgPF(Duration(1, "s")) {
          case connection: Connection ⇒ {
            connection shouldEqual mockConnection
            ref ! ConnectionPoolActor.ReturnConnection(connection)
          }
        }
        ref ! ConnectionPoolActor.RequestConnection
        expectMsg(mockConnection)
      }
    }
  }
}